#include "sensorDistancia.h"

sensorDistancia::sensorDistancia(int triggerPin, int echoPin, int distanciaMaxima)
			: sensor(triggerPin, echoPin, distanciaMaxima)
		{
			distanciaMaxima=distanciaMaxima;
		}
		
unsigned int sensorDistancia::obtenerDistancia()
		{
			int distancia = sensor.ping_cm();
			if (distancia <= 0)
				return distanciaMaxima;
			return distancia;
		}
