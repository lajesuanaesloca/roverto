/**
 * @file logging.h
 * @brief Función que logea información sobre la distancia que detecta el sensor. Modificada a partir de la de Miguel Grinberg.
 * @author Miguel Grinberg
 * Está sacado de este sitio: https://github.com/miguelgrinberg/michelino/blob/v0.2/logging.h
 */
#include <Arduino.h>
#include <stdarg.h>

inline void loguear(char const* format, ...)
{
    char line[1024];
    va_list args;
    va_start(args, format);
    vsnprintf(line, sizeof(line), format, args);
    va_end(args);
    Serial.print(line);
}
asfd�flkajds

da�sfdslkafjds�lkfakj
