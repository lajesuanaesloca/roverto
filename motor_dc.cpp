
#include "motor_dc.h"
#include "Arduino.h"

Motor::Motor(uint8_t enable, uint8_t avance, uint8_t retroceso)
{
	pinEnable=enable;
	pinAvance=avance;
	pinRetroceso=retroceso;
	pinMode(pinEnable, OUTPUT);
	pinMode(pinAvance, OUTPUT);
	pinMode(pinRetroceso, OUTPUT);
}

/**
 * Para habilitar el motor, es necesario poner en alto el pin Enable del L293D
 */
void Motor::enable()
{
	digitalWrite(pinEnable, HIGH);
	loguear("Estoy en motor::enable y pinEnable es %i\n", pinEnable);
}

/**
 * Para desabilidar el motor, es necesario poner en bajo el pin Enable del L293D.
 * El motor puede serguir girando por la inercia. 
 * De esta manera, desabilitarlo produce un frenado suave.
 */
void Motor::disable()
{
	digitalWrite(pinEnable, LOW);
	loguear("Estoy en motor::enable y pinEnable es %i\n", pinEnable);
}

/**
 * Hace girar el motor en un sentido. Para que represente un avance real en el rover, hay que prestar atención a cómo se 
 * conectan los pines al L293D.
 * Para establecer el sentido se pone en alto uno de los dos pines de salida, y en bajo el otro. En la función retroceso() 
 * simplemente se inverte el estado de estos dos pines.
 */
void Motor::avance()
{
	digitalWrite(pinAvance, HIGH);
	digitalWrite(pinRetroceso, LOW);
	loguear("Estoy en motor::avance y pinAvance es %i\n", pinAvance);
	loguear("Estoy en motor::avance y pinRetroceso es %i\n", pinRetroceso);
}

/**
 * Hace girar el motor en sentido inverso al de avance(), colocando los pines de salida en estados inversos a los de esta función.
 */ 
void Motor::retroceso()
{
	digitalWrite(pinAvance, LOW);
	digitalWrite(pinRetroceso, HIGH);
	loguear("Estoy en motor::retroceso");
}
/**
 * Lo frena de manera brusca, colocando ambos pines en el mismo nivel.
 * Elijo poner ambos en LOW porque me parece más coherente, pero ambos en HIGH produciría el mismo efecto.
 */ 
void Motor::freno()
{
	digitalWrite(pinAvance, LOW);
	digitalWrite(pinRetroceso, LOW);
	loguear("Estoy en motor::freno");
}

