#ifndef _ultrasonido_
#define _ultrasonido_
#include <NewPing.h>
#endif

/** Funciones de alto nivel del sensor de ultrasonido.
 * 
 * Encapsulo las funciones de NewPing en esta clase de mayor nivel.
 */
 
class sensorDistancia
{
	private:
		NewPing sensor; //!< Instancia de NewPing para manejar el sensor SR-04
		unsigned int distanciaMaxima; //!< distancia máxima medible. Más allá de eso se considera fuera de rango del sensor.
		
	public:
		sensorDistancia(int triggerPin, int ecoPin, int distanciaMaxima);
		unsigned int obtenerDistancia();		
};

