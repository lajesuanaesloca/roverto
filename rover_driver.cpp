#include "rover_driver.h"


/** Constructor del roobt.
 * 
 * Usando una lista de inicialización, asigno los pines. Estos pines están definidos en ino
 * Dentro del constructor, inicializo las variables propias del rover, en este caso, determino qué distancias
 * se considerarán cerca y muy cerca para frenar el rover. Las distancias se indican en cm.
 */
Rover::Rover(): motorIzquierdo(PIN_MOTOR_IZQ_ENABLE,PIN_MOTOR_IZQ_ADELANTE,PIN_MOTOR_IZQ_ATRAS),
				motorDerecho(PIN_MOTOR_DER_ENABLE,PIN_MOTOR_DER_ADELANTE,PIN_MOTOR_DER_ATRAS),
				sensor(PIN_TRIGGER, PIN_ECO, distanciaMaxima),
				distanciaPromedio(distanciaMaxima)
		 {	muyCerca=5;
			cerca=20;
            /** Defino el PIN_LED como salida, para que sirva para encender el led */
            pinMode(PIN_LED, OUTPUT);
		 }

/**
 * Para avanzar habilita todos los motores y los hace girar en el mismo sentido.
 */
void Rover::avance()
{
	loguear("Estoy en Rover::avance \n");
	motorIzquierdo.enable();
	motorDerecho.enable();
	motorIzquierdo.avance();
	motorDerecho.avance();
    estado=andando;
}
/**
 * Para retrocede habilita todos los motores y los hace girar en el mismo sentido, pero opuesto a avance()
 */
void Rover::retroceso()
{
	loguear("Estoy en Rover::retroceso \n");
	motorIzquierdo.enable();
	motorDerecho.enable();
	motorIzquierdo.retroceso();
	motorDerecho.retroceso();
    estado=andando;
}
/**
 * Para girar a la izquierda pone en retroceso las ruedas de la izquierda y en avace las de la derecha.
 * Gira sobre su propio centro.
 */
void Rover::giroIzquierda()
{
	motorIzquierdo.enable();
	motorDerecho.enable();
	motorIzquierdo.retroceso();
	motorDerecho.avance();
}
/**
 * Para girar a la izquierda pone en retroceso las ruedas de la derecha y en avace las de la izquierda.
 * Gira sobre su propio centro.
 */
void Rover::giroDerecha()
{
	motorIzquierdo.enable();
	motorDerecho.enable();
	motorIzquierdo.avance();
	motorDerecho.retroceso();
}
/**
 * Para frenar, freno todos los motores.
 */
void Rover::freno()
{
	motorIzquierdo.enable();
	motorDerecho.enable();
	motorIzquierdo.freno();
	motorDerecho.freno();
    estado=parado;
}
/**
 * Para ponerlo en punto muerto desabilito todos los motores y el rover frenará despacio, cuando se le termine el envión.
 */
void Rover::puntoMuerto()
{
	motorDerecho.disable();
	motorIzquierdo.disable();
    estado=parado;
}


/**
 * Rutina de testeo completa. Debería loguearla de alguna manera.
 */
void Rover::test()
{
    digitalWrite(PIN_LED,HIGH);
    delay(500);
    digitalWrite(PIN_LED,LOW);
    avance();
    delay(3000);
    puntoMuerto();
    delay(1000);
    retroceso();
    delay(3000);
    puntoMuerto();
    delay(1000);
    avance();
    delay(3000);
    freno();
    delay(1000);
    giroDerecha();
    delay(3000);
    puntoMuerto();
    delay(1000);
    giroIzquierda();
    delay(3000);
    puntoMuerto();
    delay(1000);
    digitalWrite(PIN_LED,HIGH);
    delay(500);
    digitalWrite(PIN_LED,LOW);
    delay(500);
    digitalWrite(PIN_LED,HIGH);
    delay(500);
    digitalWrite(PIN_LED,LOW);
    delay(500);
    digitalWrite(PIN_LED,HIGH);
    delay(500);
    digitalWrite(PIN_LED,LOW);
    delay(500);
}

/** Sortea un sentido de giro y pone el rover a girar.
*/
void Rover::girar()
{
	int sentido = random(2);
	if (sentido==0) {
		giroIzquierda();
	}
	else giroDerecha();
	estado=girando;
}


