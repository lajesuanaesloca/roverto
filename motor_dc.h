#include <Arduino.h>
#include "logging.h"
/** Define las funciones básicas de un motor de corriente continua.
 * 
 * Se asume el uso de un H-driver como el L293D para manejar el motor.
 * Basado en la clase motor_driver del proyecto Michelino ( https://github.com/miguelgrinberg/michelino)
 */
class Motor
{
	private:
	
		uint8_t pinEnable;
		uint8_t pinAvance;
		uint8_t pinRetroceso;
	
	public:		
		Motor(uint8_t enable, uint8_t avance, uint8_t retroceso);
		void enable(); //!< Habilita el motor.
		void disable(); //!< Desabilita el motor.
		void avance(); //!< Lo hace girar en un sentido
		void retroceso(); //!< Lo hace girar en sentido inverso a avance()
		void freno();	//!< Frena el motor
};

