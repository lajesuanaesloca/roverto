/**
 * @file roverto.ino
 * @brief Rutinas para testeo de las funciones.
 * @author Calu
 */

//#include "logging.h"



#ifndef _ultrasonido_
#define _ultrasonido_
#include <NewPing.h>
#endif

#ifndef _rover_
#define _rover_
#include "rover_driver.h"
#endif




Rover roverto; //!< Creo un rover, llamado roverto.

void setup() {
    	//roverto.avance();	
    	 Serial.begin(9600);
    	 roverto.avance();
}

/* Elijo el comportamiento que quiero, llamando a la función correspondiente.
 * apagado[shape="box", style=rounded, label="Está apagado"];
 *               encendido[shape="diamond", label="¿Apretaron botón de encendido?"];
 */
void loop() {
	
//digitalWrite(PIN_MOTOR_IZQ_ENABLE, HIGH);
//loguear("hola \n");
	}

/**
         \dot
         digraph {
              label="Flujo principal del programa"
              
              avance[shape="box", style=rounded, label="Avanza"];
              chequeo[shape="diamond", style=rounded, label="¿Detecta obstáculo?"];
              muyCerca[shape="diamond", style=rounded, label="¿Está muy cerca?"];
              cerca[shape="diamond", style=rounded, label="¿Está cerca?"];
              frenoBrusco[shape="box", style=rounded, label="Freno en seco"];
              frenoSuave[shape="box", style=rounded, label="Freno tranquilo"];
              sorteoSentido[label="Sorteo sentido de giro"];
              giro;
              avance -> chequeo[weight=4];
              chequeo -> avance[label="No"];
              chequeo -> muyCerca[weight=4, label = "Sí"];
              muyCerca -> cerca[weight=4, label="No"];
              muyCerca -> frenoBrusco[label="¡Sí!"];
              cerca -> frenoSuave[label = "Sí"];
              frenoSuave -> sorteoSentido;
              
              frenoBrusco -> retrocedo;
              retrocedo -> sorteoSentido;
              sorteoSentido -> giro;
              giro -> avance;
                        {rank=same; muyCerca frenoBrusco}
                        {rank=same; cerca frenoSuave}
              }
          \enddot
*/




