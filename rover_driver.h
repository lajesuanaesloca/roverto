#define PIN_MOTOR_IZQ_ENABLE 3 //!< Porque es una salida PWM
#define PIN_MOTOR_IZQ_ADELANTE 2
#define PIN_MOTOR_IZQ_ATRAS 4 //!< Ver bien qué pin es este.
#define PIN_MOTOR_DER_ENABLE 10 //!< Porque es una salida PWM
#define PIN_MOTOR_DER_ADELANTE 5
#define PIN_MOTOR_DER_ATRAS 6
#define PIN_TRIGGER 7
#define PIN_ECO 8
#define PIN_LED 9



#ifndef motor_h
#define motor_h
#include "motor_dc.h"
#endif

#ifndef _sensor_
#define _sensor_
#include "sensorDistancia.h"
#endif

#include "moving_average.h"

/** Funciones básicas para manejar un rover con arduino
 * 
 *  El rover debe tener al menos una rueda izquierda y una derecha, con movimientos independientes entre sí.
 *  Si tiene cuatro ruedas, las dos derechas se moverán siempre juntas y las izquierdas siempre juntas.
 *  No hay ruedas que giren, sino que los giros se realizan invirtiendo el sentido de rotación en el lado correspondiente.
 */

class Rover
{
	private:
		Motor motorIzquierdo, motorDerecho; //!< Representan el motor (o los motores) de cada lado.
        unsigned int distanciaObstaculo; //!< almacena la distancia detectada cada vez que mira con su ssensor
        unsigned int muyCerca;
        unsigned int cerca;
        enum state_t { parado, andando, girando }; //!< enumeración de los estados en que se puede encontrar el robot 
        state_t estado; //!< Guarda el estado actual del robot en cada ciclo.
        sensorDistancia sensor;
        unsigned int distanciaMaxima;
        MovingAverage<unsigned int, 3> distanciaPromedio;
        //Logger loggerRover;
        
	public:
		Rover();
		void avance(); //!< El rover avanza
		void retroceso();//!< El rover retrocede
		void giroIzquierda(); //!< El rover gira a la izquierda
		void giroDerecha(); //!<El rover gira a la derecha
		void freno(); //!<Frena de golpe.
		void puntoMuerto();//!< Frena suave.
        //void andar();
        void girar();
        void test();
        
        
};
